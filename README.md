# Project description
[Etsy open api](https://www.etsy.com/developers/documentation)

This is a sample project that demonstrates modern approach of Android application development using [Kotlin](https://kotlinlang.org/) and latest stack.

## Project characteristics

Tools and solutions:

* [Kotlin](https://kotlinlang.org/)
* Modern Architecture
* [Android Jetpack](https://developer.android.com/jetpack)
* A single-activity architecture
* Reactive UI
* Dependency Injection
* Material design

Tech-stack:

* [Kotlin](https://kotlinlang.org/)
* [Coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html) - background tasks
* [Corbind](https://ldralighieri.github.io/Corbind/) - Coroutines binding APIs for widgets
* [Kodein](https://kodein.org/Kodein-DI/) - DI
* [Room Persistence Library](https://developer.android.com/topic/libraries/architecture/room) - DB
* [Retrofit](https://square.github.io/retrofit/) - network interaction
* [Jetpack](https://developer.android.com/jetpack)
    * [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)
    * [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
    * [Lifecycle](https://developer.android.com/topic/libraries/architecture/lifecycle)
* [Coil](https://coil-kt.github.io/coil/) - image loading
* [Lottie](http://airbnb.io/lottie) - common animations
* [Shimmer](http://facebook.github.io/shimmer-android/) - skeleton layout animation
* [Stetho](http://facebook.github.io/stetho/) - debugging tool
* [Biometric API](https://source.android.com/security/biometric) - For user authentication

* Architecture
    * `Clean Architecture`
    * `MVVM` + `MVI`
    * [Android Architecture components](https://developer.android.com/topic/libraries/architecture)