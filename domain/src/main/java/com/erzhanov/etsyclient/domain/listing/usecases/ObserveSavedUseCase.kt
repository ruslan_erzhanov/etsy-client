package com.erzhanov.etsyclient.domain.listing.usecases

import com.erzhanov.etsyclient.domain.listing.GoodsRepository
import com.erzhanov.etsyclient.domain.listing.model.GoodsItem
import kotlinx.coroutines.flow.Flow

interface ObserveSavedUseCase {
    fun savedFlow(): Flow<List<GoodsItem>>
}

internal class ObserveSavedUseCaseImpl(private val repository: GoodsRepository): ObserveSavedUseCase {
    override fun savedFlow() = repository.savedFlow()
}