package com.erzhanov.etsyclient.domain.categories.model


data class Category(
        val name: String,
        val shortName: String
)


