package com.erzhanov.etsyclient.domain.categories

import com.erzhanov.etsyclient.domain.categories.model.Category

interface CategoriesRepository {
    suspend fun categories(): List<Category>
}