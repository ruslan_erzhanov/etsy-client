package com.erzhanov.etsyclient.domain.listing.usecases

import com.erzhanov.etsyclient.domain.listing.GoodsRepository
import com.erzhanov.etsyclient.domain.listing.model.GoodsItem

interface RemoteItemUseCase {
    suspend fun get(listingId: Long, shouldUpdate: Boolean = false): GoodsItem?
}

internal class RemoteItemUseCaseImpl(private val repository: GoodsRepository): RemoteItemUseCase {
    override suspend fun get(listingId: Long, shouldUpdate: Boolean) =
            repository.remoteItem(listingId)?.also { if (shouldUpdate) repository.update(it) }
}