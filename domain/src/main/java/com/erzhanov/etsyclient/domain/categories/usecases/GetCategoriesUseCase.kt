package com.erzhanov.etsyclient.domain.categories.usecases

import com.erzhanov.etsyclient.domain.categories.CategoriesRepository
import com.erzhanov.etsyclient.domain.categories.model.Category

interface GetCategoriesUseCase {
    suspend fun categories(): List<Category>
}

internal class GetCategoriesUseCaseImpl(private val repository: CategoriesRepository): GetCategoriesUseCase {
    override suspend fun categories() = repository.categories()
}