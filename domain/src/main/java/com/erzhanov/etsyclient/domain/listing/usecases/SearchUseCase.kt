package com.erzhanov.etsyclient.domain.listing.usecases

import com.erzhanov.etsyclient.domain.listing.GoodsRepository
import com.erzhanov.etsyclient.domain.listing.model.SearchPage

interface SearchUseCase {
    suspend fun search(page: Int, categoryName: String? = null, keywords: String? = null): SearchPage
}

internal class SearchUseCaseImpl(private val repository: GoodsRepository): SearchUseCase {

    override suspend fun search(page: Int, categoryName: String?, keywords: String?)
            = repository.remoteSearch(page, categoryName, keywords)

}