package com.erzhanov.etsyclient.domain.listing.usecases

import com.erzhanov.etsyclient.domain.listing.GoodsRepository

interface IsSavedUseCase {
    suspend fun isSaved(listingId: Long): Boolean
}

internal class IsSavedUseCaseImpl(private val repository: GoodsRepository): IsSavedUseCase {
    override suspend fun isSaved(listingId: Long) = repository.isSaved(listingId)
}