package com.erzhanov.etsyclient.domain.listing.usecases

import com.erzhanov.etsyclient.domain.listing.GoodsRepository
import com.erzhanov.etsyclient.domain.listing.model.GoodsItem

interface SaveItemUseCase {
    suspend fun save(good: GoodsItem)
}

internal class SaveItemUseCaseImpl(private val repository: GoodsRepository): SaveItemUseCase {
    override suspend fun save(good: GoodsItem) = repository.save(good)
}