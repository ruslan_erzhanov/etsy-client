package com.erzhanov.etsyclient.base.base.viewmodel

sealed class ProgressEvent {
    object Show : ProgressEvent()
    object Hide : ProgressEvent()
}