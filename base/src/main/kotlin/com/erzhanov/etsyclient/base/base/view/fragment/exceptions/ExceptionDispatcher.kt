package com.erzhanov.etsyclient.base.base.view.fragment.exceptions

interface ExceptionDispatcher {

    fun dispatch(exception: Throwable)
}
