package com.erzhanov.etsyclient.base.base.view.fragment


interface StateHandler<in T> {
    fun onStateChanged(state: T)
}