package com.erzhanov.etsyclient.base.base.view.activity

interface ConnectionLostHandler {
    fun onConnectionLost() {}
}