package com.erzhanov.etsyclient.base.base.view.fragment

import org.kodein.di.Kodein

interface FragmentModuleHolder {

    val fragmentModule: Kodein.Module
}