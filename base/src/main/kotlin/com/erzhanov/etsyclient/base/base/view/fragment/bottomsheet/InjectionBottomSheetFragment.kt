package com.erzhanov.etsyclient.base.base.view.fragment.bottomsheet

import androidx.fragment.app.Fragment
import com.erzhanov.etsyclient.base.base.view.fragment.FragmentModuleHolder
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.kcontext


abstract class InjectionBottomSheetFragment : BottomSheetDialogFragment(), KodeinAware, FragmentModuleHolder {

    @SuppressWarnings("LeakingThisInConstructor")
    final override val kodeinContext = kcontext<Fragment>(context = this)

    final override val kodein: Kodein = Kodein.lazy {
        val parentKodein by closestKodein(requireNotNull(activity))
        extend(parentKodein, allowOverride = true)
        import(fragmentModule, allowOverride = true)
    }

}
