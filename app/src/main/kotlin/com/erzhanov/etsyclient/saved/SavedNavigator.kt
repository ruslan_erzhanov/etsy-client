package com.erzhanov.etsyclient.saved

import androidx.fragment.app.FragmentManager
import com.erzhanov.etsyclient.base.navigator.BaseNavigator
import com.erzhanov.etsyclient.details.DetailsFragment
import com.erzhanov.etsyclient.main.bottomsheet.SaveRemoveBottomSheet
import com.erzhanov.etsyclient.search.model.AppGoodsItem
import java.util.concurrent.atomic.AtomicReference

interface SavedNavigator : BaseNavigator {

    fun details(id: Long) {
        manager.get()?.to(fragment = DetailsFragment.saved(id))
    }

    fun bottomSheet(item: AppGoodsItem) {
        SaveRemoveBottomSheet.saved(item).apply { show(manager.get(), this::class.java.name) }
    }
}

internal class SavedNavigatorImpl : SavedNavigator {
    override val manager = AtomicReference<FragmentManager>()
}