package com.erzhanov.etsyclient.saved

import android.os.Parcelable
import com.erzhanov.etsyclient.base.base.viewmodel.BaseAction
import com.erzhanov.etsyclient.base.base.viewmodel.BaseViewModel
import com.erzhanov.etsyclient.base.base.viewmodel.BaseViewState
import com.erzhanov.etsyclient.base.mappers.mapList
import com.erzhanov.etsyclient.base.viewmodel.BaseViewModelImpl
import com.erzhanov.etsyclient.domain.listing.model.GoodsItem
import com.erzhanov.etsyclient.domain.listing.usecases.ObserveSavedUseCase
import com.erzhanov.etsyclient.domain.listing.usecases.RemoveAllUseCase
import com.erzhanov.etsyclient.saved.ActionSaved.UpdateLoaded
import com.erzhanov.etsyclient.search.model.toApp
import kotlinx.coroutines.flow.onEach


interface SavedViewModel : BaseViewModel {
    fun removeAll()
}

internal class SavedViewModelImpl(
        private val savedUseCase: ObserveSavedUseCase,
        private val removeAllUseCase: RemoveAllUseCase
) : SavedViewModel, BaseViewModelImpl<StateSaved, ActionSaved>(StateSaved()) {

    override fun onCreate() {
        super.onCreate()
        if (state.noState) loadData()
    }

    override fun onLoadData() {
        savedUseCase.savedFlow().onEach { sendAction(UpdateLoaded(it.process())) }.bind()
    }

    override fun onReduceState(action: ActionSaved) =
            when (action) {
                is UpdateLoaded -> state.copy(items = action.items)
                else -> StateSaved()
            }

    override fun removeAll() {
        launchOn { removeAllUseCase.remove() }
    }

    private fun List<GoodsItem>.process() = mapList(GoodsItem::toApp)

}

internal data class StateSaved(
        val items: List<Parcelable> = emptyList()
) : BaseViewState {
    val noState = items.isEmpty()
}

internal sealed class ActionSaved : BaseAction {
    data class UpdateLoaded(val items: List<Parcelable>) : ActionSaved()
}