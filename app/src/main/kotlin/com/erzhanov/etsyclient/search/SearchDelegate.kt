package com.erzhanov.etsyclient.search

import android.os.Parcelable
import android.view.View
import androidx.fragment.app.Fragment
import com.erzhanov.etsyclient.BR
import com.erzhanov.etsyclient.R
import com.erzhanov.etsyclient.base.base.list.AsyncDiffObservableList
import com.erzhanov.etsyclient.base.base.list.paging.PagingAttacher
import com.erzhanov.etsyclient.base.base.list.paging.clearPaging
import com.erzhanov.etsyclient.base.base.list.paging.paging
import com.erzhanov.etsyclient.base.base.livedata.SingleLiveEvent
import com.erzhanov.etsyclient.base.base.livedata.unit
import com.erzhanov.etsyclient.base.extentions.*
import com.erzhanov.etsyclient.base.view.delegate.ViewDelegate
import com.erzhanov.etsyclient.databinding.ItemAppSearchResultBinding
import com.erzhanov.etsyclient.search.model.AppGoodsItem
import com.github.nitrico.lastadapter.LastAdapter
import kotlinx.android.synthetic.main.search_fragment.*
import kotlinx.coroutines.flow.onEach
import ru.ldralighieri.corbind.swiperefreshlayout.refreshes

class SearchDelegate : ViewDelegate() {

    val nextPage = SingleLiveEvent<Unit>()
    val refresh = SingleLiveEvent<Unit>()
    val details = SingleLiveEvent<Long>()
    val saveRequest = SingleLiveEvent<AppGoodsItem>()
    private val data = AsyncDiffObservableList<Parcelable>().skeleton()
    private val adapter = LastAdapter(data, BR.item).setup()
    private lateinit var attacher: PagingAttacher

    override fun postCreateView(fragment: Fragment, view: View) {
        super.postCreateView(fragment, view)
        searchResults.set(adapter)
        attacher = searchResults.paging { nextPage.unit() }
        swipe.refreshes().onEach { refresh.unit() }.bind()
    }

    fun onLoaded(items: List<Parcelable>) {
        swipe.hide()
        data.update(items)
    }

    override fun preDestroyView(fragment: Fragment, view: View) {
        super.preDestroyView(fragment, view)
        searchResults.clear()
        searchResults.clearPaging(attacher)
    }

    private fun LastAdapter.setup() = apply {
        map<AppSkeletonItem>(R.layout.item_app_skeleton)
        map<AppGoodsItem, ItemAppSearchResultBinding>(R.layout.item_app_search_result) {
            onClick { it.binding.item?.listingId?.run(details::postValue) }
            onLongClick { it.binding.item?.run(saveRequest::postValue) }
        }
    }
}