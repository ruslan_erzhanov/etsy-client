package com.erzhanov.etsyclient.search

import androidx.fragment.app.FragmentManager
import com.erzhanov.etsyclient.base.navigator.BaseNavigator
import com.erzhanov.etsyclient.details.DetailsFragment
import com.erzhanov.etsyclient.main.bottomsheet.SaveRemoveBottomSheet
import com.erzhanov.etsyclient.search.model.AppGoodsItem
import java.util.concurrent.atomic.AtomicReference

interface SearchNavigator : BaseNavigator {

    fun details(id: Long) {
        manager.get()?.to(fragment = DetailsFragment.search(id))
    }

    fun bottomSheet(item: AppGoodsItem) {
        SaveRemoveBottomSheet.search(item).apply { show(manager.get(), this::class.java.name) }
    }
}

internal class SearchNavigatorImpl : SearchNavigator {
    override val manager = AtomicReference<FragmentManager>()
}