package com.erzhanov.etsyclient.details

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DetailsArgs(
        val id: Long,
        val flow: DetailFlow
) : Parcelable

enum class DetailFlow { FROM_SEARCH, FROM_SAVED }