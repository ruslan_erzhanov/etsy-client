package com.erzhanov.etsyclient.details

import androidx.fragment.app.FragmentManager
import com.erzhanov.etsyclient.base.navigator.BaseNavigator
import java.util.concurrent.atomic.AtomicReference

interface DetailsNavigator: BaseNavigator

internal class DetailsNavigatorImpl: DetailsNavigator {
    override val manager = AtomicReference<FragmentManager>()
}