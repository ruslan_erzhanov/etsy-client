package com.erzhanov.etsyclient.splash

import androidx.lifecycle.LiveData
import com.erzhanov.etsyclient.base.base.livedata.SingleLiveEvent
import com.erzhanov.etsyclient.base.base.livedata.unit
import com.erzhanov.etsyclient.base.base.viewmodel.BaseAction
import com.erzhanov.etsyclient.base.base.viewmodel.BaseViewModel
import com.erzhanov.etsyclient.base.base.viewmodel.BaseViewState
import com.erzhanov.etsyclient.base.viewmodel.BaseViewModelImpl
import com.erzhanov.etsyclient.splash.ActionSplash.DataLoadSuccess
import kotlinx.coroutines.delay

interface SplashViewModel : BaseViewModel {
    val toSignIn: LiveData<Unit>
    val toMain: LiveData<Unit>
}

internal class SplashViewModelImpl : SplashViewModel, BaseViewModelImpl<StateSplash, ActionSplash>(StateSplash()) {

    override val toSignIn = SingleLiveEvent<Unit>()
    override val toMain = SingleLiveEvent<Unit>()

    override fun onLoadData() {
        launchOn {
            delay(1000)
            sendAction(DataLoadSuccess)
            toMain.unit()
        }
    }

    override fun onReduceState(action: ActionSplash) =
            when (action) {
                is DataLoadSuccess -> state.copy(isLoading = false)
                else -> throw IllegalStateException("not implemented yet")
            }
}

internal sealed class ActionSplash : BaseAction {
    object DataLoadSuccess : ActionSplash()
    object DataLoadFailure : ActionSplash()
}

internal data class StateSplash(
        val isLoading: Boolean = true
) : BaseViewState