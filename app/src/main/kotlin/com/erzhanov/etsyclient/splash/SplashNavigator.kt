package com.erzhanov.etsyclient.splash

import androidx.fragment.app.FragmentManager
import com.erzhanov.etsyclient.base.navigator.BaseNavigator
import com.erzhanov.etsyclient.main.fragment.MainFragment
import java.util.concurrent.atomic.AtomicReference

interface SplashNavigator : BaseNavigator {
    fun signIn() {}
    fun main() = manager.get()?.go(fragment = MainFragment())
}

internal class SplashNavigatorImpl : SplashNavigator {
    override val manager = AtomicReference<FragmentManager>()
}