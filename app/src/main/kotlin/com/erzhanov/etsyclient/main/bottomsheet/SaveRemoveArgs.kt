package com.erzhanov.etsyclient.main.bottomsheet

import android.os.Parcelable
import com.erzhanov.etsyclient.search.model.AppGoodsItem
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SaveRemoveArgs(
        val item: AppGoodsItem,
        val flow: SaveRemoveFlow
): Parcelable

enum class SaveRemoveFlow { FROM_SEARCH, FROM_SAVED }