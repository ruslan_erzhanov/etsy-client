package com.erzhanov.etsyclient.main.adapter

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import com.erzhanov.etsyclient.R
import com.erzhanov.etsyclient.saved.SavedFragment
import com.erzhanov.etsyclient.search.SearchFragment

enum class NavigationPage(@IdRes val id: Int, val position: Int, val lazyPage: () -> Fragment) {

    SEARCH(R.id.actionSearch, 0, lazyPage = { SearchFragment() }),
    SAVED(R.id.actionSaved, 1, lazyPage = { SavedFragment() });

    companion object {

        fun from(id: Int) = values().first { it.id == id }

        fun position(id: Int) = from(id).position

        fun find(position: Int) = values().first { it.position == position }.id
    }
}