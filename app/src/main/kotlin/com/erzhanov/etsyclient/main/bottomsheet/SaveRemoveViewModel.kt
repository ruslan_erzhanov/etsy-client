package com.erzhanov.etsyclient.main.bottomsheet

import com.erzhanov.etsyclient.base.base.viewmodel.BaseAction
import com.erzhanov.etsyclient.base.base.viewmodel.BaseViewModel
import com.erzhanov.etsyclient.base.base.viewmodel.BaseViewState
import com.erzhanov.etsyclient.base.mappers.mapItem
import com.erzhanov.etsyclient.base.viewmodel.BaseViewModelImpl
import com.erzhanov.etsyclient.domain.listing.usecases.IsSavedUseCase
import com.erzhanov.etsyclient.domain.listing.usecases.RemoveItemUseCase
import com.erzhanov.etsyclient.domain.listing.usecases.SaveItemUseCase
import com.erzhanov.etsyclient.main.bottomsheet.StateSaveRemove.NoState
import com.erzhanov.etsyclient.search.model.AppGoodsItem
import com.erzhanov.etsyclient.search.model.toDomain


interface SaveRemoveViewModel : BaseViewModel {
    fun remove()
    fun save()
}

internal class SaveRemoveViewModelImpl(
        private val isSavedUseCase: IsSavedUseCase,
        private val removeItemUseCase: RemoveItemUseCase,
        private val saveItemUseCase: SaveItemUseCase
) : SaveRemoveViewModel, BaseViewModelImpl<StateSaveRemove, ActionSaveRemove>(NoState) {

    private val saveRemoveArgs by lazy { getArgs<SaveRemoveArgs>() }

    private val flow get() = saveRemoveArgs.flow
    private val item get() = saveRemoveArgs.item

    override fun onCreate() {
        super.onCreate()
        if (state is NoState) loadData()
    }

    override fun onLoadData() {
        launchOn {
            val action = when (flow) {
                SaveRemoveFlow.FROM_SEARCH -> ActionSaveRemove.ItemStatusLoaded(shouldSave = isSavedUseCase.isSaved(item.listingId).not())
                SaveRemoveFlow.FROM_SAVED -> ActionSaveRemove.ItemStatusLoaded(shouldSave = false)
            }
            sendAction(action)
        }
    }

    override fun onReduceState(action: ActionSaveRemove) =
            when (action) {
                is ActionSaveRemove.ItemStatusLoaded -> StateSaveRemove.StatusLoadedState(shouldSave = action.shouldSave, shouldRemove = action.shouldSave.not())
                is ActionSaveRemove.ItemSaved -> StateSaveRemove.ItemSaved
                is ActionSaveRemove.ItemRemoved -> StateSaveRemove.ItemRemoved
            }

    override fun remove() {
        launchOn {
            removeItemUseCase.remove(item.listingId)
            sendAction(ActionSaveRemove.ItemRemoved)
        }
    }

    override fun save() {
        launchOn {
            saveItemUseCase.save(item.mapItem(AppGoodsItem::toDomain))
            sendAction(ActionSaveRemove.ItemSaved)
        }
    }
}

internal sealed class ActionSaveRemove : BaseAction {
    data class ItemStatusLoaded(val shouldSave: Boolean = false) : ActionSaveRemove()
    object ItemSaved : ActionSaveRemove()
    object ItemRemoved : ActionSaveRemove()
}

internal sealed class StateSaveRemove : BaseViewState {
    data class StatusLoadedState(val shouldSave: Boolean = false, val shouldRemove: Boolean = false) : StateSaveRemove()
    object ItemSaved : StateSaveRemove()
    object ItemRemoved : StateSaveRemove()
    object NoState : StateSaveRemove()
}

