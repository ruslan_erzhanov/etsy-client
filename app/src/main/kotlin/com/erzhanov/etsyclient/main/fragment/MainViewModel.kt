package com.erzhanov.etsyclient.main.fragment

import com.erzhanov.etsyclient.base.base.viewmodel.BaseAction
import com.erzhanov.etsyclient.base.base.viewmodel.BaseViewModel
import com.erzhanov.etsyclient.base.base.viewmodel.BaseViewState
import com.erzhanov.etsyclient.base.viewmodel.BaseViewModelImpl


interface MainViewModel : BaseViewModel

internal class MainViewModelImpl : MainViewModel, BaseViewModelImpl<StateMain, ActionMain>(StateMain.NoState) {

    override fun onLoadData() {}

    override fun onReduceState(action: ActionMain) = StateMain.NoState
}

internal sealed class StateMain : BaseViewState {
    object NoState: StateMain()
}

internal sealed class ActionMain : BaseAction {
    object NoAction: ActionMain()
}