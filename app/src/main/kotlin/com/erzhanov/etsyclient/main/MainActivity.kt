package com.erzhanov.etsyclient.main

import com.erzhanov.etsyclient.R
import com.erzhanov.etsyclient.base.view.BaseActivity
import com.erzhanov.etsyclient.splash.SplashFragment


class MainActivity : BaseActivity() {
    override val isMainRoot = true
    override val layoutResId: Int get() = R.layout.activity_main
    override fun start() = manager.get().go(fragment = SplashFragment())
}