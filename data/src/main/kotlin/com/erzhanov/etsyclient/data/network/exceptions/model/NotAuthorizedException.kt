package com.erzhanov.etsyclient.data.network.exceptions.model


class NotAuthorizedException(cause: Throwable): NetworkException(cause)