package com.erzhanov.etsyclient.data.network.exceptions.model

class NetworkDisableException(exception: Throwable): NetworkException(exception)