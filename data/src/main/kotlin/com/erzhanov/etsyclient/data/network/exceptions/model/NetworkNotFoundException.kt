package com.erzhanov.etsyclient.data.network.exceptions.model

class NetworkNotFoundException(throwable: Throwable): NetworkException(throwable)