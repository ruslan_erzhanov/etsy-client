package com.erzhanov.etsyclient.data.categories

import com.erzhanov.etsyclient.base.mappers.mapList
import com.erzhanov.etsyclient.data.categories.model.api.ApiCategory
import com.erzhanov.etsyclient.data.categories.model.api.toDomain
import com.erzhanov.etsyclient.domain.categories.CategoriesRepository
import com.erzhanov.etsyclient.domain.categories.model.Category



class CategoriesRepositoryImpl(private val taxonomyApi: TaxonomyApi) : CategoriesRepository {

    override suspend fun categories(): List<Category> {
        return taxonomyApi.getCategories()?.results.mapList(ApiCategory::toDomain)
    }

}