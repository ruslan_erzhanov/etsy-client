package com.erzhanov.etsyclient.data.categories


import com.erzhanov.etsyclient.data.ApiContract
import com.erzhanov.etsyclient.data.categories.model.api.ApiCategories
import retrofit2.http.GET

interface TaxonomyApi {

    @GET(ApiContract.Taxonomy.CATEGORIES)
    suspend fun getCategories(): ApiCategories?

}