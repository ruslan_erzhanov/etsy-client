package com.erzhanov.etsyclient.data.network.exceptions.model

class NetworkInternalException(throwable: Throwable): NetworkException(throwable)